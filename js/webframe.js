/* global variables */
var repititionCount = 0;
var sliderLoopCount = 0;
var currentSlide = 0;
var fadeInTimer;
var fadeOutTimer;
var slideInTimer;
var slideOutTimer;

/* function starts here */
function setSlider(sliderItem) {
        varslider1 = document.getElementsByClassName(sliderItem.name)[0];
        slideCount = slider1.childElementCount;
        slide = slider1.children;
        currentSlide = 0;
        if (sliderItem.mode == "fade") {
            webframeFadeInItem(slide[currentSlide], 2000);
        }
        if (sliderItem.mode = "slide") {
            webframeSlideInLeftItem(slide[currentSlide], slide[currentSlide + 1], 2000);
        }
    }
    /*=====================================slide effect block========================================*/
    /*------------------------slide in Item--------------------------------*/
function webframeSlideInLeftItem(currentItem, nextItem, timeInterval) {
    var loopCount = (timeInterval / 1000) * 50;
    sliderLoopCount = 0;
    currentItem.style.display = "block";
    currentItem.style.left = "0%";
    currentItem.style.zIndex = "99";
    nextItem.style.display = "block";
    nextItem.style.zIndex = "95";
    //nextItem.style.left="100%";
    slideInLeftLoopCount = 0;
    slideOutLeftLoopCount = 0;
    //webframeSlideInLeft(nextItem, loopCount, 20);
    webframeSlideOutLeft(currentItem, loopCount, 20);
}

function webframeSlideInLeft(slideItem, loopCount, slideInterval) {
    slideInLeftLoopCount++;
    slideValue = ((loopCount - slideInLeftLoopCount) / loopCount) * 100;
    slideInTimer = setTimeout(function () {
        slideItem.style.left = slideValue.toString() + "%";
        if (slideInLeftLoopCount <= loopCount) {
            webframeSlideInLeft(slideItem, loopCount, slideInterval);
        } else {
            slideInLeftLoopCount = 0;
            slideItem.style.display = "block";
        }
    }, slideInterval);
}

function webframeSlideOutLeft(slideItem, loopCount, slideInterval) {
        slideOutLeftLoopCount++;
        slideValue = (slideOutLeftLoopCount / loopCount) * 100;
        slideInTimer = setTimeout(function () {
            slideItem.style.left = slideValue.toString() + "%";
            if (slideOutLeftLoopCount <= loopCount) {
                webframeSlideOutLeft(slideItem, loopCount, slideInterval);
            } else {
                slideOutLeftLoopCount = 0;
                slideItem.style.display = "none";
            }
        }, slideInterval);
    }
    /**
     * [[fade BLock starts here==================================================]]
     */
function prevSlideFadeIn() {
    clearTimeout(fadeInTimer);
    clearTimeout(fadeOutTimer);
    webframeFadeOutItem(slide[currentSlide], 800);
    currentSlide--;
    if (currentSlide < 0) {
        currentSlide = slideCount - 1;
    }
    setTimeout(function () {
        webframeFadeInItem(slide[currentSlide], 2000);
    }, 1000);
}

function nextSlideFadeIn() {
        clearTimeout(fadeInTimer);
        clearTimeout(fadeOutTimer);
        webframeFadeOutItem(slide[currentSlide], 800);
        currentSlide++;
        if (currentSlide == slideCount) {
            currentSlide = 0;
        }
        setTimeout(function () {
            webframeFadeInItem(slide[currentSlide], 2000);
        }, 1000);
    }
    /*------------------------fade In Item--------------------------------*/
function webframeFadeInItem(item, timeInterval) {
    var loopCount = (timeInterval / 1000) * 50;
    sliderLoopCount = 0;
    item.style.display = "block";
    item.style.opacity = "0.0";
    webframeFadeIn(item, loopCount, 20);
}

function webframeFadeIn(slideItem, loopCount, slideInterval) {
        sliderLoopCount++;
        opacityValue = sliderLoopCount / loopCount;
        fadeInTimer = setTimeout(function () {
            slideItem.style.opacity = opacityValue.toString();
            if (sliderLoopCount <= loopCount) {
                webframeFadeIn(slideItem, loopCount, slideInterval);
            } else {
                sliderLoopCount = 0;
            }
        }, slideInterval);
    }
    /*------------------------fade Out Item--------------------------------*/
function webframeFadeOutItem(item, timeInterval) {
    var loopCount = (timeInterval / 1000) * 50;
    sliderLoopCount = 0;
    item.style.display = "block";
    //    item.style.opacity = "1";
    item.style.opacity = opacityValue.toString();
    webframeFadeOut(item, loopCount, 20);
}

function webframeFadeOut(slideItem, loopCount, slideInterval) {
        sliderLoopCount++;
        opacityValue = (loopCount - sliderLoopCount) / loopCount;
        fadeOutTimer = setTimeout(function () {
            slideItem.style.opacity = opacityValue.toString();
            if (sliderLoopCount <= loopCount) {
                webframeFadeOut(slideItem, loopCount, slideInterval);
            } else {
                sliderLoopCount = 0;
                slideItem.style.display = "none";
            }
        }, slideInterval);
    }
    /*=====================================Fade effect block=========================================*/
    /**
     * @param {Object} timer [[Description]]
     *                       <script>
                                    var timer1 = {
                                        dateString: "Jun 26 2016",  --> time till date
                                        dayElement: "days-remaining1",  --> day element should be id
                                        hourElement: "hours-remaining1",    --> hours element should be id
                                        minuteElement: "minutes-remaining1",    -->minutes element should be id
                                        secondElement: "seconds-remaining1"     --> seconds element should be id
                                    };
                                    flashTimer(timer1);     --> object definition
                                </script>
     */
function flashTimer(timer) {
        setInterval(function () {
            var d1 = new Date();
            var d2 = new Date(timer.dateString);
            var seconds1 = parseInt(d1.getTime() / 1000);
            var seconds2 = parseInt(d2.getTime() / 1000);
            var secondsRemaining = seconds2 - seconds1;
            var minutesRemaining = parseInt(secondsRemaining / 60);
            var hoursRemaining = parseInt(minutesRemaining / 60);
            var days = parseInt(hoursRemaining / 24);
            var hours = hoursRemaining - (days * 24);
            var minutes = minutesRemaining - ((days * 24 * 60) + (hours * 60));
            var seconds = secondsRemaining - ((days * 24 * 3600) + (hours * 3600) + (minutes * 60));
            document.getElementById(timer.dayElement).innerHTML = days.toString() + " Days";
            document.getElementById(timer.hourElement).innerHTML = hours.toString();
            document.getElementById(timer.minuteElement).innerHTML = minutes.toString();
            document.getElementById(timer.secondElement).innerHTML = seconds.toString();
        }, 1000);
    }
    /*=====================================slide left block=========================================*/

/*
    code to slide left in or out of a dom object
    just use toggleSlideLeft function in you code
*/

var leftMenuCount = 0;

function toggleSlideLeft(item, inLimit, outLimit, time) {
    if (leftMenuCount == 0) {
        itemSlideInLeft(item, inLimit, time);
        leftMenuCount++;
    } else {
        itemSlideOutLeft(item, outLimit, time);
        leftMenuCount = 0;
    }
}

function itemSlideInLeft(item, limit, time) {
    var leftpos = item.offsetLeft;
    var steps = parseInt(leftpos / (time * (-40))); //steps=totalSteps/(timeNeedes/frquency)
    slideInLeft(item, leftpos, steps, limit);
}

function slideInLeft(item, loopStart, steps, limit) {
    var slideValue = loopStart + steps;
    var slideInTimer = setTimeout(function () {
        item.style.left = slideValue.toString() + "px";
        if (loopStart < limit) {
            loopStart = slideValue;
            slideInLeft(item, loopStart, steps, limit);
        }
    }, 25); //value 25 for 40hz.
}

function itemSlideOutLeft(item, limit, time) {
    var leftpos = item.offsetLeft;
    var steps = parseInt(leftpos / (time * 40)); //steps=totalSteps/(timeNeedes/frquency)
    slideOutLeft(item, leftpos, steps, limit);
}

function slideOutLeft(item, loopStart, steps, limit) {
        var slideValue = loopStart - steps;
        var slideOutTimer = setTimeout(function () {
            item.style.left = slideValue.toString() + "px";
            if (loopStart > limit) {
                loopStart = slideValue;
                slideOutLeft(item, loopStart, steps, limit);
            }
        }, 25); //value 25 for 40hz.
    }
    /*=====================================slide right block=========================================*/

/*
    code to slide right in or out of a dom object
    just use toggleSlideRight function in you code
*/

var rightMenuCount = 0;

function toggleSlideRight(item, inLimit, outLimit, time) {
    if (rightMenuCount == 0) {
        itemSlideInRight(item, inLimit, time);
        rightMenuCount++;
    } else {
        itemSlideOutRight(item, outLimit, time);
        rightMenuCount = 0;
    }
}

function itemSlideInRight(item, limit, time) {
    var rightpos = window.innerWidth - (item.offsetLeft + item.offsetWidth);
    var steps = parseInt(rightpos / (time * (-40))); //steps=totalSteps/(timeNeedes/frquency)
    slideInRight(item, rightpos, steps, limit);
}

function slideInRight(item, loopStart, steps, limit) {
    var slideValue = loopStart + steps;
    var slideInTimer = setTimeout(function () {
        item.style.right = slideValue.toString() + "px";
        if (loopStart < limit) {
            loopStart = slideValue;
            slideInRight(item, loopStart, steps, limit);
        }
    }, 25); //value 25 for 40hz.
}

function itemSlideOutRight(item, limit, time) {
    var rightpos = window.innerWidth - (item.offsetLeft + item.offsetWidth);
    var steps = parseInt(rightpos / (time * 40)); //steps=totalSteps/(timeNeedes/frquency)
    slideOutRight(item, rightpos, steps, limit);
}

function slideOutRight(item, loopStart, steps, limit) {
    var slideValue = loopStart - steps;
    var slideOutTimer = setTimeout(function () {
        item.style.right = slideValue.toString() + "px";
        if (loopStart > limit) {
            loopStart = slideValue;
            slideOutRight(item, loopStart, steps, limit);
        }
    }, 25); //value 25 for 40hz.
}

/*=====================================slide top block=========================================*/

/*
    code to slide top in or out of a dom object
    just use toggleSlideTop function in you code
*/
var topMenuCount = 0;

function toggleSlideTop(item, inLimit, outLimit, time) {
    if (topMenuCount == 0) {
        itemSlideInTop(item, inLimit, time);
        topMenuCount++;
    } else {
        itemSlideOutTop(item, outLimit, time);
        topMenuCount = 0;
    }
}

function itemSlideInTop(item, limit, time) {
    var toppos = item.offsetTop;
    var steps = parseInt(toppos / (time * (-40))); //steps=totalSteps/(timeNeedes/frquency)
    slideInTop(item, toppos, steps, limit);
}

function slideInTop(item, loopStart, steps, limit) {
    var slideValue = loopStart + steps;
    var slideInTimer = setTimeout(function () {
        item.style.top = slideValue.toString() + "px";
        if (loopStart < limit) {
            loopStart = slideValue;
            slideInTop(item, loopStart, steps, limit);
        }
    }, 25); //value 25 for 40hz.
}

function itemSlideOutTop(item, limit, time) {
    var toppos = item.offsetTop;
    var steps = parseInt(toppos / (time * 40)); //steps=totalSteps/(timeNeedes/frquency)
    slideOutTop(item, toppos, steps, limit);
}

function slideOutTop(item, loopStart, steps, limit) {
        var slideValue = loopStart - steps;
        var slideOutTimer = setTimeout(function () {
            item.style.top = slideValue.toString() + "px";
            if (loopStart > limit) {
                loopStart = slideValue;
                slideOutTop(item, loopStart, steps, limit);
            }
        }, 25); //value 25 for 40hz.
    }
    /*=====================================slide bottom block=========================================*/

/*
    code to slide bottom in or out of a dom object
    just use toggleSlideBottom function in you code
*/
var bottomMenuCount = 0;

function toggleSlideBottom(item, inLimit, outLimit, time) {
    if (bottomMenuCount == 0) {
        itemSlideInBottom(item, inLimit, time);
        bottomMenuCount++;
    } else {
        itemSlideOutBottom(item, outLimit, time);
        bottomMenuCount = 0;
    }
}

function itemSlideInBottom(item, limit, time) {
    var bottompos = window.innerHeight - (item.offsetTop + item.offsetHeight);
    var steps = parseInt(bottompos / (time * (-40))); //steps=totalSteps/(timeNeedes/frquency)
    slideInBottom(item, bottompos, steps, limit);
}

function slideInBottom(item, loopStart, steps, limit) {
    var slideValue = loopStart + steps;
    var slideInTimer = setTimeout(function () {
        item.style.bottom = slideValue.toString() + "px";
        if (loopStart < limit) {
            loopStart = slideValue;
            slideInBottom(item, loopStart, steps, limit);
        }
    }, 25); //value 25 for 40hz.
}

function itemSlideOutBottom(item, limit, time) {
    var bottompos = window.innerHeight - (item.offsetTop + item.offsetHeight);
    var steps = parseInt(bottompos / (time * 40)); //steps=totalSteps/(timeNeedes/frquency)
    slideOutBottom(item, bottompos, steps, limit);
}

function slideOutBottom(item, loopStart, steps, limit) {
    var slideValue = loopStart - steps;
    var slideOutTimer = setTimeout(function () {
        item.style.bottom = slideValue.toString() + "px";
        if (loopStart > limit) {
            loopStart = slideValue;
            slideOutBottom(item, loopStart, steps, limit);
        }
    }, 25); //value 25 for 40hz.
}
